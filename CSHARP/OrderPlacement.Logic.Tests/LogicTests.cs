﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using OrderPlacement.Data;
    using OrderPlacement.Logic;
    using OrderPlacement.Repository.Interfaces;

    /// <summary>
    /// Testing of all the logic
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        private Mock<IAddressesRepository> mockAddresses;
        private Mock<IMembersRepository> mockMembers;
        private Mock<IOrdersRepository> mockOrders;
        private Mock<IStocksRepository> mockStocks;
        private AddressesLogic aLogic;
        private MembersLogic mLogic;
        private OrdersLogic oLogic;
        private StocksLogic sLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicTests"/> class.
        /// </summary>
        public LogicTests()
        {
            // Arrange
            this.mockAddresses = new Mock<IAddressesRepository>();
            this.mockMembers = new Mock<IMembersRepository>();
            this.mockOrders = new Mock<IOrdersRepository>();
            this.mockStocks = new Mock<IStocksRepository>();
            List<Addresses> addresses = new List<Addresses>()
            {
                new Addresses() { AddressID = 1, AddressDescription = "Aulich", City = "Bratislava", Country = "Szlovákia", NumberFrom = "29", NumberTo = "23", Province = "Pozsonyi", Street = "Kozia", StreetExtension = "utca", ZIP = "811" },
                new Addresses() { AddressID = 2, AddressDescription = "Török", City = "Gödöllő", Country = "Magyarország", NumberFrom = "2", NumberTo = "4", Province = "Pest", Street = "Röges", StreetExtension = "utca", ZIP = "2100" },
                new Addresses() { AddressID = 3, AddressDescription = "Lahner", City = "Necpál", Country = "Szlovákia", NumberFrom = "170", Province = "Zsolnai", Street = "Necpaly", StreetExtension = "utca", ZIP = "038 12" },
                new Addresses() { AddressID = 4, AddressDescription = "Schweidel", City = "Zombor", Country = "Szerbia", NumberFrom = "1", Province = "Vajdaság", Street = "Kosovska", StreetExtension = "utca", ZIP = "25000" },
                new Addresses() { AddressID = 5, AddressDescription = "Pöltenberg", City = "Bécs", Country = "Ausztria", NumberFrom = "1", Province = "Wien", Street = "Löwelstraße", StreetExtension = "straße", ZIP = "11010" },
                new Addresses() { AddressID = 6, AddressDescription = "Nagysándor", City = "Nagyvárad", Country = "Románia", NumberFrom = "46", NumberTo = "40", Province = "Bihar", Street = "Spartacus", StreetExtension = "strada", ZIP = "410469" },
                new Addresses() { AddressID = 7, AddressDescription = "Knézics", City = "Nagygordonya", Country = "Horvátország", NumberFrom = "4a", Province = "Belovár-Bilogora", Street = "Petra Preradovića", StreetExtension = "ulica", ZIP = "43270" },
                new Addresses() { AddressID = 8, AddressDescription = "Leiningen", City = "Niddatal", Country = "Németország", NumberFrom = "10", Province = "Hessen", Street = "Bahnhofstraße", StreetExtension = "straße", ZIP = "61194" },
                new Addresses() { AddressID = 9, AddressDescription = "Dessewffy", City = "Ósvacsákány", Country = "Szlovákia", NumberFrom = "64", Province = "Kassai", Street = "Čakanovce", StreetExtension = "utca", ZIP = "044 45" },
                new Addresses() { AddressID = 10, AddressDescription = "Damjanich", City = "Sztáza", Country = "Horvátország", NumberFrom = "79a", Province = "Sziszek-Moslavina", Street = "Novoselska", StreetExtension = "utca", ZIP = "44210" },
                new Addresses() { AddressID = 11, AddressDescription = "Lázár", City = "Zrenjanin", Country = "Szerbia", NumberFrom = "23", Province = "Vajdaság", Street = "Nikole Tesle", StreetExtension = "utca", ZIP = "23000" },
                new Addresses() { AddressID = 12, AddressDescription = "Vécsey", City = "Rzeczniów", Country = "Lengyelország", NumberFrom = "2", Province = "Mazóvia", Street = "Rzeczniówek", StreetExtension = "utca", ZIP = "27-353" },
                new Addresses() { AddressID = 13, AddressDescription = "Kiss", City = "Temesvár", Country = "Románia", NumberFrom = "67", NumberTo = "79", Province = "Temes", Street = "Eroilor de la Tisa", StreetExtension = "Bulevardul", ZIP = "300030" },
            };
            List<Members> members = new List<Members>()
            {
                new Members() { MemberID = 1, AddressID = 1, Prefix = string.Empty, FirstName = "Aulich", LastName = "Lajos", Birthdate = new DateTime(int.Parse("19430825".Substring(0, 4)), int.Parse("19430825".Substring(4, 2)), int.Parse("19430825".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 2, AddressID = 2, Prefix = string.Empty, FirstName = "Török", LastName = "Ignác", Birthdate = new DateTime(int.Parse("19450723".Substring(0, 4)), int.Parse("19450723".Substring(4, 2)), int.Parse("19450723".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 3, AddressID = 3, Prefix = string.Empty, FirstName = "Lahner", LastName = "György", Birthdate = new DateTime(int.Parse("19451006".Substring(0, 4)), int.Parse("19451006".Substring(4, 2)), int.Parse("19451006".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 4, AddressID = 4, Prefix = string.Empty, FirstName = "Schweidel", LastName = "József", Birthdate = new DateTime(int.Parse("19460518".Substring(0, 4)), int.Parse("19460518".Substring(4, 2)), int.Parse("19460518".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 5, AddressID = 5, Prefix = "Lovag", FirstName = "Poelt von Poeltenberg", LastName = "Ernő", Birthdate = new DateTime(int.Parse("19580220".Substring(0, 4)), int.Parse("19580220".Substring(4, 2)), int.Parse("19580220".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 6, AddressID = 6, Prefix = string.Empty, FirstName = "Nagysándor", LastName = "József", Birthdate = new DateTime(int.Parse("19530819".Substring(0, 4)), int.Parse("19530819".Substring(4, 2)), int.Parse("19530819".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 7, AddressID = 7, Prefix = string.Empty, FirstName = "Knezić", LastName = "Károly", Birthdate = new DateTime(int.Parse("19580906".Substring(0, 4)), int.Parse("19580906".Substring(4, 2)), int.Parse("19580906".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 8, AddressID = 8, Prefix = "Gróf", FirstName = "Leiningen-Westerburg", LastName = "Károly", Birthdate = new DateTime(int.Parse("19690411".Substring(0, 4)), int.Parse("19690411".Substring(4, 2)), int.Parse("19690411".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 9, AddressID = 9, Prefix = string.Empty, FirstName = "Dessewffy", LastName = "Arisztid", Birthdate = new DateTime(int.Parse("19520702".Substring(0, 4)), int.Parse("19520702".Substring(4, 2)), int.Parse("19520702".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 10, AddressID = 10, Prefix = string.Empty, FirstName = "Damjanich", LastName = "János", Birthdate = new DateTime(int.Parse("19541208".Substring(0, 4)), int.Parse("19541208".Substring(4, 2)), int.Parse("19541208".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 11, AddressID = 11, Prefix = string.Empty, FirstName = "Lázár", LastName = "Vilmos", Birthdate = new DateTime(int.Parse("19671024".Substring(0, 4)), int.Parse("19671024".Substring(4, 2)), int.Parse("19671024".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 12, AddressID = 12, Prefix = "Gróf", FirstName = "Vécsey", LastName = "Károly", Birthdate = new DateTime(int.Parse("19531124".Substring(0, 4)), int.Parse("19531124".Substring(4, 2)), int.Parse("19531124".Substring(6, 2))), Status = "D" },
                new Members() { MemberID = 13, AddressID = 13, Prefix = string.Empty, FirstName = "Kiss", LastName = "Ernő", Birthdate = new DateTime(int.Parse("19490713".Substring(0, 4)), int.Parse("19490713".Substring(4, 2)), int.Parse("19490713".Substring(6, 2))), Status = "D" },
            };
            List<Orders> orders = new List<Orders>()
            {
                new Orders() { OrdersID = 1, MemberID = 1, ItemID = 1, OrderState = "F", GrossValue = 2500, VAT = 2500 * 0.27, NetValue = 2500 * 1.27 },
                new Orders() { OrdersID = 2, MemberID = 2, ItemID = 2, OrderState = "F", GrossValue = 4000, VAT = 4000 * 0.27, NetValue = 4000 * 1.27 },
                new Orders() { OrdersID = 3, MemberID = 3, ItemID = 3, OrderState = "F", GrossValue = 3490, VAT = 3490 * 0.27, NetValue = 3490 * 1.27 },
                new Orders() { OrdersID = 4, MemberID = 4, ItemID = 4, OrderState = "W", GrossValue = 1000, VAT = 1000 * 0.27, NetValue = 1000 * 1.27 },
                new Orders() { OrdersID = 5, MemberID = 5, ItemID = 5, OrderState = "W", GrossValue = 2500, VAT = 2500 * 0.27, NetValue = 2500 * 1.27 },
                new Orders() { OrdersID = 6, MemberID = 6, ItemID = 6, OrderState = "F", GrossValue = 4999, VAT = 4999 * 0.27, NetValue = 4999 * 1.27 },
                new Orders() { OrdersID = 7, MemberID = 7, ItemID = 7, OrderState = "P", GrossValue = 6000, VAT = 6000 * 0.27, NetValue = 6000 * 1.27 },
                new Orders() { OrdersID = 8, MemberID = 8, ItemID = 8, OrderState = "F", GrossValue = 1200, VAT = 1200 * 0.27, NetValue = 1200 * 1.27 },
                new Orders() { OrdersID = 9, MemberID = 9, ItemID = 9, OrderState = "P", GrossValue = 25000, VAT = 25000 * 0.27, NetValue = 25000 * 1.27 },
                new Orders() { OrdersID = 10, MemberID = 10, ItemID = 10, OrderState = "F", GrossValue = 5999, VAT = 999 * 0.27, NetValue = 999 * 1.27 },
                new Orders() { OrdersID = 11, MemberID = 11, ItemID = 11, OrderState = "P", GrossValue = 129999, VAT = 129999 * 0.27, NetValue = 129999 * 1.27 },
                new Orders() { OrdersID = 12, MemberID = 12, ItemID = 12, OrderState = "F", GrossValue = 4899, VAT = 4899 * 0.27, NetValue = 4899 * 1.27 },
                new Orders() { OrdersID = 13, MemberID = 13, ItemID = 13, OrderState = "F", GrossValue = 41999, VAT = 41999 * 0.27, NetValue = 41999 * 1.27 },
            };
            List<Stocks> stocks = new List<Stocks>()
            {
                new Stocks() { ItemID = 1, ExpectedShipTimeInDays = 3, ItemDescription = "Lámpa", StockAmount = 10 },
                new Stocks() { ItemID = 2, ExpectedShipTimeInDays = 1, ItemDescription = "Asztal", StockAmount = 13 },
                new Stocks() { ItemID = 3, ExpectedShipTimeInDays = 2, ItemDescription = "Éjjeli szekrény", StockAmount = 7 },
                new Stocks() { ItemID = 4, ExpectedShipTimeInDays = 6, ItemDescription = "Komód", StockAmount = 4 },
                new Stocks() { ItemID = 5, ExpectedShipTimeInDays = 12, ItemDescription = "Párna", StockAmount = 72 },
                new Stocks() { ItemID = 6, ExpectedShipTimeInDays = 9, ItemDescription = "Paplan", StockAmount = 6 },
                new Stocks() { ItemID = 7, ExpectedShipTimeInDays = 2, ItemDescription = "Ágy", StockAmount = 8 },
                new Stocks() { ItemID = 8, ExpectedShipTimeInDays = 7, ItemDescription = "Lepedő", StockAmount = 42 },
                new Stocks() { ItemID = 9, ExpectedShipTimeInDays = 11, ItemDescription = "Matrac", StockAmount = 17 },
                new Stocks() { ItemID = 10, ExpectedShipTimeInDays = 7, ItemDescription = "Üres doboz", StockAmount = 26 },
                new Stocks() { ItemID = 11, ExpectedShipTimeInDays = 4, ItemDescription = "Megégett melegszendvics", StockAmount = 73 },
                new Stocks() { ItemID = 12, ExpectedShipTimeInDays = 6, ItemDescription = "Mérgezett alma", StockAmount = 26 },
                new Stocks() { ItemID = 13, ExpectedShipTimeInDays = 5, ItemDescription = "Gyufásdoboz", StockAmount = 134 },
            };
            this.mockAddresses.Setup(m => m.GetAll()).Returns(addresses.AsQueryable);
            this.aLogic = new AddressesLogic(this.mockAddresses.Object);

            this.mockMembers.Setup(m => m.GetAll()).Returns(members.AsQueryable);
            this.mLogic = new MembersLogic(this.mockMembers.Object);

            this.mockOrders.Setup(m => m.GetAll()).Returns(orders.AsQueryable);
            this.oLogic = new OrdersLogic(this.mockOrders.Object);

            this.mockStocks.Setup(m => m.GetAll()).Returns(stocks.AsQueryable);
            this.sLogic = new StocksLogic(this.mockStocks.Object);
        }

        /// <summary>
        /// Gets enumerable list for stocks testing
        /// </summary>
        /// <returns>List of stocks</returns>
        public static List<string> GetStocksVal()
        {
            List<string> output = new List<string>();

            output.Add("Asztal");
            output.Add("szekrény");
            output.Add("égett");
            output.Add("Gyufás");
            output.Add("li sz");

            return output;
        }

        /// <summary>
        /// Gets enumerable list for members testing
        /// </summary>
        /// <returns>List of members</returns>
        public static List<string> GetMembersVal()
        {
            List<string> output = new List<string>();

            output.Add("Poelt von Poeltenberg");
            output.Add("Leiningen-Westerburg");
            output.Add("Gróf");
            output.Add("Lovag");
            output.Add("Schweidel");
            output.Add("Dessewffy");

            return output;
        }

        /// <summary>
        /// Gets enumerable list for orders testing
        /// </summary>
        /// <returns>List of orders</returns>
        public static List<string> GetOrdersVal()
        {
            List<string> output = new List<string>();

            output.Add("129999");
            output.Add("5999");
            output.Add("41999");
            output.Add("F");
            output.Add("W");
            output.Add("P");

            return output;
        }

        /// <summary>
        /// Gets enumerable list for addresses testing
        /// </summary>
        /// <returns>List of addresses</returns>
        public static List<string> GetAddressesVal()
        {
            List<string> output = new List<string>();

            output.Add("27-353");
            output.Add("Németország");
            output.Add("Rzeczniów");
            output.Add("Necpál");
            output.Add("Ósvacsákány");
            output.Add("Čakanovce");
            output.Add("410469");

            return output;
        }

        /// <summary>
        /// Testing stocks logic
        /// </summary>
        /// <param name="val">Input parameter</param>
        [TestCaseSource(nameof(GetStocksVal))]
        public void TestStocksContains(string val)
        {
            // Act
            var list = this.sLogic.List();

            // Assert
            Assert.That(list.Where(x => x.Contains(val)).First(), Is.Not.Null);
            Assert.That(list.Count, Is.EqualTo(13));
            this.mockStocks.Verify(m => m.GetAll(), Times.Exactly(GetStocksVal().IndexOf(val) + 1));
            this.mockStocks.Verify(m => m.Insert(It.IsAny<Stocks>()), Times.Never);
            this.mockStocks.Verify(m => m.Remove(It.IsAny<Stocks>()), Times.Never);
            this.mockStocks.Verify(m => m.Change(It.IsAny<Stocks>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Testing members logic
        /// </summary>
        /// <param name="val">Input parameter</param>
        [TestCaseSource(nameof(GetMembersVal))]
        public void TestMembersContains(string val)
        {
            // Act
            var list = this.mLogic.List();

            // Assert
            Assert.That(list.Count, Is.EqualTo(13));
            Assert.That(list.Where(x => x.Contains(val)).First(), Is.Not.Null);
            this.mockMembers.Verify(m => m.GetAll(), Times.Exactly(GetMembersVal().IndexOf(val) + 1));
            this.mockMembers.Verify(m => m.Insert(It.IsAny<Members>()), Times.Never);
            this.mockMembers.Verify(m => m.Remove(It.IsAny<Members>()), Times.Never);
            this.mockMembers.Verify(m => m.Change(It.IsAny<Members>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Testing orders logic
        /// </summary>
        /// <param name="val">Input parameter</param>
        [TestCaseSource(nameof(GetOrdersVal))]
        public void TestOrdersContains(string val)
        {
            // Act
            var list = this.oLogic.List();

            // Assert
            Assert.That(list.Count, Is.EqualTo(13));
            Assert.That(list.Where(x => x.Contains(val)).First(), Is.Not.Null);
            this.mockOrders.Verify(m => m.GetAll(), Times.Exactly(GetOrdersVal().IndexOf(val) + 1));
            this.mockOrders.Verify(m => m.Insert(It.IsAny<Orders>()), Times.Never);
            this.mockOrders.Verify(m => m.Remove(It.IsAny<Orders>()), Times.Never);
            this.mockOrders.Verify(m => m.Change(It.IsAny<Orders>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Testing addresses logic
        /// </summary>
        /// <param name="val">Input parameter</param>
        [TestCaseSource(nameof(GetAddressesVal))]
        public void TestAddressesContains(string val)
        {
            // Act
            var list = this.aLogic.List();

            // Assert
            Assert.That(list.Count, Is.EqualTo(13));
            Assert.That(list.Where(x => x.Contains(val)).First(), Is.Not.Null);
            this.mockAddresses.Verify(m => m.GetAll(), Times.Exactly(GetAddressesVal().IndexOf(val) + 1));
            this.mockAddresses.Verify(m => m.Insert(It.IsAny<Addresses>()), Times.Never);
            this.mockAddresses.Verify(m => m.Remove(It.IsAny<Addresses>()), Times.Never);
            this.mockAddresses.Verify(m => m.Change(It.IsAny<Addresses>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }
    }
}
