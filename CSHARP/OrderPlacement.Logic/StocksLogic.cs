﻿// <copyright file="StocksLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Logic
{
    using System;
    using System.Linq;
    using OrderPlacement.Data;
    using OrderPlacement.Repository;
    using OrderPlacement.Repository.Interfaces;

    /// <summary>
    /// Logic behind handling Stocks
    /// </summary>
    public class StocksLogic
    {
        private IStocksRepository stocksRepository = new StocksRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="StocksLogic"/> class.
        /// </summary>
        public StocksLogic()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StocksLogic"/> class.
        /// </summary>
        /// <param name="stocksRepository">Repository to access data</param>
        public StocksLogic(IStocksRepository stocksRepository)
        {
            this.stocksRepository = stocksRepository ?? throw new ArgumentNullException(nameof(stocksRepository));
        }

        /// <summary>
        /// Creates a new entity
        /// </summary>
        /// <param name="param">Inserted parameters</param>
        public void New(string[] param)
        {
            Stocks stocks = new Stocks();
            for (int i = 0; i < 3; i++)
            {
                if (param[i] != null && !param[i].Trim(' ').Equals(string.Empty))
                {
                    switch (i)
                    {
                        case 0:
                            try
                            {
                                stocks.ExpectedShipTimeInDays = int.Parse(param[i]);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                stocks = null;
                                i = 3;
                                break;
                            }

                            continue;
                        case 1:
                            stocks.ItemDescription = param[i];
                            continue;
                        case 2:
                            try
                            {
                                stocks.StockAmount = int.Parse(param[i]);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                stocks = null;
                                i = 3;
                                break;
                            }

                            continue;
                        default:
                            break;
                    }
                }
            }

            if (stocks != null)
            {
                this.Add(stocks);
            }
            else
            {
                Console.WriteLine("Nem sikerült az új készlet hozzáadása!");
                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                {
                }
            }
        }

        /// <summary>
        /// Lists shit
        /// </summary>
        /// <returns>All stocks</returns>
        public string[] List()
        {
            var q = from w in this.stocksRepository.GetAll()
                    select w;
            string[] res = new string[q.Count()];
            int i = 0;
            foreach (Stocks v in q)
            {
                string str = string.Empty;

                str = $"{v.ItemID} {v.ExpectedShipTimeInDays} ";

                if (v.ItemDescription != null && !v.ItemDescription.Equals(string.Empty))
                {
                    str += v.ItemDescription + " ";
                }
                else
                {
                    str += " null ";
                }

                str += v.StockAmount + " ";
                res[i] = str;
                i++;
            }

            return res;
        }

        /// <summary>
        /// Removing a stock
        /// </summary>
        /// <param name="id">Id of removed stock</param>
        public void Remove(int id)
        {
            try
            {
                Stocks stocks = this.stocksRepository.GetAll().Where(x => x.ItemID == id).Single();
                this.stocksRepository.Remove(stocks);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (e.InnerException != null)
                {
                    if (e.InnerException.InnerException != null)
                    {
                        Console.WriteLine(e.InnerException.InnerException.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Changes an entity in the table
        /// </summary>
        /// <param name="number">ID of entity</param>
        /// <param name="field">Number of field</param>
        /// <param name="newfield">Value of field's new value</param>
        public void Change(string number, string field, string newfield)
        {
            string fie = string.Empty;
            switch (int.Parse(field))
            {
                case 1:
                    fie = "ExpectedShipTimeInDays";
                    break;
                case 2:
                    fie = "ItemDescription";
                    break;
                case 3:
                    fie = "StockAmount";
                    break;
                default:
                    break;
            }

            Stocks stocks = this.stocksRepository.GetAll().Where(w => w.ItemID.ToString().Equals(number)).Single();
            this.stocksRepository.Change(stocks, fie, newfield);
        }

        /// <summary>
        /// Gets a parameter from a member
        /// </summary>
        /// <param name="id">Input ID</param>
        /// <param name="field">Parameter to find</param>
        /// <returns>Found parameter</returns>
        public string GetParameter(int id, string field)
        {
            string rst = string.Empty;
            switch (field)
            {
                case "ExpectedShipTimeInDays":
                    rst = this.stocksRepository.GetAll().Where(x => x.ItemID == id).Single().ExpectedShipTimeInDays.ToString();
                    break;
                case "ItemDescription":
                    rst = this.stocksRepository.GetAll().Where(x => x.ItemID == id).Single().ItemDescription.ToString();
                    break;
                case "StockAmount":
                    rst = this.stocksRepository.GetAll().Where(x => x.ItemID == id).Single().StockAmount.ToString();
                    break;
                default:
                    break;
            }

            return rst;
        }

        /// <summary>
        /// Adds a new stock to the table
        /// </summary>
        /// <param name="stocks">Input stock</param>
        private void Add(Stocks stocks)
        {
            this.stocksRepository.Insert(stocks);
        }
    }
}
