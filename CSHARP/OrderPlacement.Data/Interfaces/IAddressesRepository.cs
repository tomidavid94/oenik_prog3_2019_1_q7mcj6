﻿// <copyright file="IAddressesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Repository.Interfaces
{
    using OrderPlacement.Data;

    /// <summary>
    /// Interface for accessing Addresses repository
    /// </summary>
    public interface IAddressesRepository : IRepository<Addresses>
    {
    }
}
