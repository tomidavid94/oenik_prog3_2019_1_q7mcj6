/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author I330322
 */
public class Szemely implements Serializable{

    public Szemely(String id, String desc, int val) {
        this.id = id;
        this.desc = desc;
        this.values = new int[5];
        for (int i = 0; i < values.length; i++) {
            values[i] = (int)(((Math.random() / 2) + 0.75) * val);
        }
    }

    private final String id;
    private final String desc;
    private final int[] values;
    
    public String getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public int getVal(int i) {
        return values[i];
    }
}
