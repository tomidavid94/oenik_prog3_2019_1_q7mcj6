<%--
Views should be stored under the WEB-INF folder so that
they are not accessible except through controller process.

This JSP is here to provide a redirect to the dispatcher
servlet but should be the only JSP outside of WEB-INF.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% response.sendRedirect("index.htm"); %>

<%--<root>
   <first>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(0) %></val>
   </first>
   <second>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(1) %></val>
   </second>
   <third>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(2) %></val>
   </third>
   <fourth>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(3) %></val>
   </fourth>
   <fifth>
      <id><%= szem.getId() %></id>
      <desc><%= szem.getDesc() %></desc>
      <val><%= szem.getVal(4) %></val>
   </fifth>
</root> --%>

<html>
   <head>
      <title>JSTL x:parse Tags</title>
   </head>

   <body>
      <h3>Books Info:</h3>
      <c:import var = "bookInfo" url="http://localhost:8080/books.xml"/>
 
      <x:parse xml = "${bookInfo}" var = "output"/>
      <b>The title of the first book is</b>: 
      <x:out select = "$output/books/book[1]/name" />
      <br>
      
      <b>The price of the second book</b>: 
      <x:out select = "$output/books/book[2]/price" />
   </body>
</html>

<%@page import="Model.*"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Számla</title>
    </head>
    <body>
        <%
            Szemely user = (Szemely) request.getSession().getAttribute("user");
        %>

        <h1>Név: <%= user.getId() %>  Neptun: <%= user.getVal() %></h1>

        <form action="Hozzaad">
            <table>
                <tr>
                    <th align="center">Tárgynév</th>
                    <th align="center">Eredmeny</th>
                </tr>
                <tr>
                    <td align="center"><input type="text" name="targy"/></td>
                    <td align="center">
                        <select name="jegy">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </td>
                    <td><input type="submit" value="Hozzáadás"/></td>
                </tr>
            </table>
        </form>
        
        <form action="Torles">
            <%
                Szemely p = (Szemely) request.getSession().getAttribute("user");          
                if (p.getVizsgak().size() > 0) {
            %>
            <table border="1">
                <tr>
                    <th>Név</th>
                    <th>Eredmeny</th>
                    <th>Torles</th>
                </tr>
                <%
                    int i = 0; 
                    for (Vizsga f : p.getVizsgak()) {
                %>
                <tr>
                    <td><%= f.getTargy() %></td>
                    <td><%= f.getEredmeny() %></td>
                    <td><a href="Torles?torles=<%= i %>">Törlés</a></td>
                </tr>
                <%
                    i++;
                   }
                %>
            </table>
            <%
                }
            %>
        </form>


        <a href="index.html">Kilépés</a>
    </body>
</html>